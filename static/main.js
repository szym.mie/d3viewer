const data_url = "/data/";
const data_list = "_data_list_.json";

async function dataFetch(url) {
    const res =  await fetch(url);
    return await res.json();
}

async function makeList() {
    const data_url_list = `${data_url}${data_list}`;
    console.log(data_url_list);
    let menu_json;
    prop = {c: 0, a: []};
    await dataFetch(data_url_list).then(res => {menu_json = res});
    console.log(menu_json);
    for (const [k, v] of Object.entries(menu_json)) {
        const e = {n: k, f: v, o: null}
        await dataFetch(`${data_url}${v}`).then(res => {e.o = res})
        prop.a.push(e);

    }
    console.log(prop);
    document.getElementById("load_screen").style.animationPlayState = "running";
    return prop
}

const goNext = ev => {
    const [_, btc] = ev.target.children;
    btc.style.visibility = "visible";
    if (dli.c < dli.a.length-1) {
        ++dli.c;
        chordDiagram(dimensions, group_dimensions, dli);
    }
    setTimeout(() => {
        btc.style.visibility = "hidden";
    }, 100);
}

const goPrev = ev => {
    const [_, btc] = ev.target.children;
    btc.style.visibility = "visible";
    if (dli.c > 0) {
        --dli.c;
        chordDiagram(dimensions, group_dimensions, dli);
    }
    setTimeout(() => {
        btc.style.visibility = "hidden";
    }, 100);
}

function makeSvg(sel, [x, y]) {
    const svg = d3
        .select(sel)
        .append("svg")
        .attr("width", x)
        .attr("height", y)
        .append("g")
        .attr("transform", `translate(${x/2}, ${y/2})`);
    return svg;
}

function initSvg(svg, [x, y]) {
    svg
        .append("g")
        .attr("transform", `translate(${x/2}, ${y/2})`);
    return svg;
}

function clearSvg(sel) {
    const svg = d3.select(sel);
}

function makeChord(dat, pad, srt) {
    const res = d3
        .chord()
        .padAngle(pad)
        .sortSubgroups(srt)
        (dat.m);
    return {r: res, d: dat};
}

function drawChords(svg, {r: res, d: dat}, [rad, _]) {
    svg
        .datum(res)
        .append("g")
        .selectAll("path")
        .data(d => d)
        .enter()
        .append("path")
        .attr("d", d3.ribbon().radius(rad))
        .style("fill", d => dat.c[d.source.index])
        .style("stroke", "black");
}

function drawGroups(svg, {r: res, d: dat}, [rad, wid]) {
    const grp = svg
        .datum(res)
        .append("g")
        .selectAll("g")
        .data(d => d.groups)
        .enter()
    grp
        .append("g")
        .append("path")
        .style("fill", (_, i) => dat.c[i])
        .style("stroke", "black")
        .attr("d", d3.arc()
            .innerRadius(rad)
            .outerRadius(rad+wid)
        );
    return grp;
}

function drawLabels(grp, {r: res, d: dat}, [rad, wid], stp) {
    grp
        .selectAll(".group-tick")
        .data(d => groupTicks(d, stp/2))
        .enter()
        .append("g")
        .attr("transform", d => `rotate(${toDeg(d.angle) - 90}) translate(${rad + wid},0)`)
        .append("line")
        .attr("x2", d => d.value % (stp) ? 3 : 6)
        .attr("stroke", "black");
    grp
        .selectAll(".group-tick-label")
        .data(d => groupTicks(d, stp))
        .enter()
        .filter(d => (d.value % stp === 0))
        .append("g")
        .attr("transform", d => `rotate(${toDeg(d.angle) - 90}) translate(${rad + wid},0)`)
        .append("text")
        .attr("x", 8)
        .attr("dy", ".35em")
        .attr("transform", d => d.angle > Math.PI  ? "rotate(180) translate(-16)" : null)
        .style("text-anchor", d => d.angle > Math.PI ? "end" : null)
        .text(d => d.value)
        .style("font-size", 9);
    grp
        .selectAll(".group-label")
        .data(d => groupLabels(d, dat))
        .enter()
        .append("g")
        .attr("transform", d => `rotate(${toDeg(d.angle) - 90}) translate(${rad + wid},0)`)
        .append("text")
        .attr("x", 36)
        .attr("dy", ".35em")
        .attr("transform", d => d.angle > Math.PI  ? "rotate(180) translate(-72)" : null)
        .style("text-anchor", d => d.angle > Math.PI ? "end" : null)
        .text(d => d.value)
        .style("font-size", 9);

    console.log(grp);
}

function chordDiagram(dim, gdi, drs) {
    const o = drs.a[drs.c].o;
    d3.select("#data_view > *").remove();
    const svg = makeSvg("#data_view", dim);
    const mix = makeChord(o, .05, d3.descending);
    console.log(mix);
    drawChords(svg, mix, gdi);
    const grp = drawGroups(svg, mix, gdi);
    drawLabels(grp, mix, gdi, o.s);
}

function groupTicks(d, stp) {
    const k = (d.endAngle - d.startAngle) / d.value;
    return d3.range(0, d.value, stp).map(v => ({value: v, angle: v * k + d.startAngle}));
}

function groupLabels(d, dat) {
    const k = (d.startAngle + d.endAngle) / 2;
    console.log(dat.n[d.index]);
    return [{value: dat.n[d.index], angle: k}]
}

const toDeg = deg => deg * 180 / Math.PI;


const mat = [
    [0, 10, 30, 15],
    [35, 0, 20, 45],
    [45, 50, 0, 55],
    [75, 5, 40, 0]
];

const col = ["#eb4034a0", "#498fc4a0", "#71b342a0", "#e0ab38a0"];
const nam = ["a", "b", "c", "d"];

const data = {m: mat, c: col, n: nam};

const dimensions = [640, 640];
const group_dimensions = [200, 10];

var dli;
makeList(dli).then(res => 
    {
        dli = res;
        chordDiagram(dimensions, group_dimensions, dli);
});

