from flask import Flask, render_template, redirect, abort
import json
app = Flask(__name__, static_folder="static", static_url_path="")

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/data/<path:file>")
def data(file):
    j = {}
    try:
        with open(f"./data/{file}") as f:
            j = json.load(f)
    except FileNotFoundError:
        print(f"/data/{file}")
        abort(404)
    return j